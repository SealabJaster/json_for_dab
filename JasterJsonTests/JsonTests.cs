﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Jaster.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Jaster.Json.Tests
{
    [TestClass()]
    public class JsonTests
    {
        [TestMethod()]
        public void JsonStringCtorTest()
        {
            var json = new Json("Hello World!");

            Assert.AreEqual(Json.Type.String, json.type);
            Assert.AreEqual("Hello World!", json.As<string>());
        }

        [TestMethod()]
        public void JsonObjectCtorTest()
        {
            var obj = new Dictionary<string, Json>();
            obj["First"] = new Json("Downvoted for spam");
            obj["Second"] = new Json(69);

            var json = new Json(obj);

            Assert.AreEqual(Json.Type.Object,     json.type);
            Assert.AreEqual("Downvoted for spam", json.Get("First").As<string>());
            Assert.AreEqual(69,                   json.Get("Second").As<int>());
        }

        [TestMethod()]
        public void JsonArrayCtorTest()
        {
            var array = new Json[] 
            {
                new Json("Hello!"),
                new Json(true)
            };

            // TODO: Might be worth making this CTOR more generic.
            var json = new Json(array.ToList());

            Assert.AreEqual(Json.Type.Array, json.type);
            Assert.AreEqual("Hello!",        json.Get(0).As<string>());
            Assert.AreEqual(true,            json.Get(1).As<bool>());
        }

        [TestMethod()]
        public void JsonBooleanCtorTest()
        {
            var json = new Json(true);

            Assert.AreEqual(Json.Type.Boolean, json.type);
            Assert.IsTrue(json.As<bool>());
        }

        [TestMethod()]
        public void JsonSignedIntCtorTest()
        {
            var json = new Json(200);

            Assert.AreEqual(Json.Type.Number, json.type);
            Assert.AreEqual(200,              json.As<int>());
        }

        [TestMethod()]
        public void JsonUnsignedIntCtorTest()
        {
            var json = new Json(3147483647U);

            Assert.AreEqual(Json.Type.Number, json.type);
            Assert.AreEqual(3147483647U,      json.As<uint>());
        }

        [TestMethod()]
        public void JsonDecimalCtorTest()
        {
            var json = new Json(420.69f);

            Assert.AreEqual(Json.Type.Number, json.type);
            Assert.AreEqual(420.69f,          json.As<float>(), 0.00001f);
        }

        [TestMethod()]
        public void ObjectSetGetTest()
        {
            var json = Json.EmptyObject;
            json.Set("Name", new Json("Bradley"));

            Assert.AreEqual("Bradley", json.Get("Name").As<string>());
            Assert.IsNull(json.Get("Pocket Rocket", default_: null));
        }

        [TestMethod()]
        public void ArrayAppendGetTest()
        {
            var json = Json.EmptyArray;
            json.Append(new Json("Dabiel"));

            Assert.AreEqual("Dabiel", json.Get(0).As<string>());
            Assert.IsNull(json.Get(20000, default_: null));
        }

        [TestMethod()]
        public void ArraySetGetTest()
        {
            var json = Json.EmptyArray;
            json.Append(new Json("Crutchle"));
            json.Set(0, new Json("Bently"));

            Assert.AreEqual("Bently", json.Get(0).As<string>());
        }

        [TestMethod()]
        public void IsNullTest()
        {
            Assert.IsTrue(Json.Null.IsNull());
        }

        [TestMethod()]
        public void GetArrayEnumeratorTest()
        {
            var json = Json.EmptyArray;
            json.Append(new Json("Hello "));
            json.Append(new Json(69));
            json.Append(new Json(" me please."));

            string buffer = "";
            foreach(Json value in json.GetArrayEnumerator())
            {
                if(value.type == Json.Type.String)
                    buffer += value.As<string>();
                else
                    buffer += Convert.ToString(value.As<int>());
            }

            Assert.AreEqual("Hello 69 me please.", buffer);
        }

        [TestMethod()]
        public void GetDictionaryEnumeratorTest()
        {
            var json = Json.EmptyObject;
            json.Set("One", new Json("Hello"));
            json.Set("Two", new Json("World"));

            string buffer = "";
            foreach(KeyValuePair<string, Json> value in json.GetDictionaryEnumerator())
            {
                buffer += $"{value.Key}: {value.Value.As<string>()}\n";
            }

            bool flag = false;
            if(buffer == "One: Hello\nTwo: World\n"
            || buffer == "Two: World\nOne: Hello\n")
                flag = true;

            Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void StressTest()
        {
            const string testFile = "../../Test.json";
            string text = File.ReadAllText(testFile);

            var json = Json.Parse(text);
            File.WriteAllText("debug.json", json.ToJsonString());
        }
    }
}